package com.example.jean.retrofitexample.view;

import com.example.jean.retrofitexample.model.history.History;
import com.example.jean.retrofitexample.model.player.Player;

import java.util.List;

public interface FootBallView {

    void playerReady(List<Player> players);
    void historyReady(List<History> histories);
}
