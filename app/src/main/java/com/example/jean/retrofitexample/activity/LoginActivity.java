package com.example.jean.retrofitexample.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.jean.retrofitexample.R;
import com.example.jean.retrofitexample.presenter.LoginPresenter;
import com.example.jean.retrofitexample.view.LoginView;

public class LoginActivity extends AppCompatActivity implements LoginView {
LoginPresenter loginPresenter;
EditText nama,password;
Button login;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        loginPresenter = new LoginPresenter(this);
        nama = findViewById(R.id.etNama);
        password = findViewById(R.id.etPassword);
    }

    @Override
    public void loginproses() {
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
    }

    public void clickLogin(View view) {
        String getNama = nama.getText().toString();
        String getPassword = password.getText().toString();
        loginPresenter.login(getApplicationContext(),getNama,getPassword);
    }

    public void gotoDaftar(View view) {
        Intent intent = new Intent(this,DaftarActivity.class);
        startActivity(intent);
    }
}
