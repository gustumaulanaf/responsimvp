package com.example.jean.retrofitexample.presenter;

import com.example.jean.retrofitexample.model.history.History;
import com.example.jean.retrofitexample.model.player.Player;
import com.example.jean.retrofitexample.model.history.ResponseHistory;
import com.example.jean.retrofitexample.model.player.RestResponse;
import com.example.jean.retrofitexample.service.BaseService;
import com.example.jean.retrofitexample.view.FootBallView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FootballPresenter {

    private FootBallView footBallView;
    private BaseService baseService;

    public FootballPresenter(FootBallView view) {
        this.footBallView = view;

        if (this.baseService == null) {
            this.baseService = new BaseService();
        }
    }

    public void getPlayerPresenter() {
        baseService
                .getAPI()
                .getPlayer()
                .enqueue(new Callback<RestResponse>() {
                    @Override
                    public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                        RestResponse data = response.body();


                        if (response.isSuccessful()){
                            List<Player> result =data.getResult();
                            footBallView.playerReady(result);
                        }
                    }

                    @Override
                    public void onFailure(Call<RestResponse> call, Throwable t) {
                        try {
                            throw new InterruptedException("Something went wrong!");
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }
    public void getHistory(){
        baseService.getAPI().getHistory().enqueue(new Callback<ResponseHistory>() {
            @Override
            public void onResponse(Call<ResponseHistory> call, Response<ResponseHistory> response) {
                ResponseHistory data = response.body();
                if (response.isSuccessful()){
                    List<History> result = data.getResult();
                    footBallView.historyReady(result);
                }
            }

            @Override
            public void onFailure(Call<ResponseHistory> call, Throwable t) {

            }
        });
    }
}
