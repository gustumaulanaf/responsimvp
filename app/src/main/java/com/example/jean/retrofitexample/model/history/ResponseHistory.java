package com.example.jean.retrofitexample.model.history;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseHistory {

    @SerializedName("success")
    private boolean success;

    @SerializedName("data")
    private List<History> result;

    public List<History> getResult() {
        return result;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}