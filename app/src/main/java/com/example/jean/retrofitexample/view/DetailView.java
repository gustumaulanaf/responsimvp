package com.example.jean.retrofitexample.view;

import com.example.jean.retrofitexample.model.history.History;
import com.example.jean.retrofitexample.model.history.HistoryItem;

import java.util.List;

public interface DetailView {
    void updateDetail (List<History> histories);
}
