package com.example.jean.retrofitexample.presenter;

import android.app.ProgressDialog;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.example.jean.retrofitexample.room.AppDatabase;
import com.example.jean.retrofitexample.room.Users;
import com.example.jean.retrofitexample.view.DaftarView;

import java.util.ArrayList;
import java.util.List;

public class DaftarPresenter {
    Users pengguna;
    List<Users> users = new ArrayList<>();
    DaftarView daftarView;
    AppDatabase db;

    public DaftarPresenter(DaftarView daftarView) {
        this.daftarView = daftarView;
    }

    public void daftar(Context context, String nama, String password) {
        if (nama.equals("")) {
            Toast.makeText(context, "Username Tidak Boleh Kosong", Toast.LENGTH_SHORT).show();
        } else if (password.equals("")) {
            Toast.makeText(context, "Password Tidak Boleh Kosong", Toast.LENGTH_SHORT).show();
        } else if (nama.equals("") && password.equals("")) {
            Toast.makeText(context, "Silahkan Diisi Semua", Toast.LENGTH_SHORT).show();
        } else if (!nama.equals("") && !password.equals("")) {
            ProgressDialog progressDialog = new ProgressDialog(context);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setMessage("Tunggu");
            progressDialog.show();
            db = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "user-db").allowMainThreadQueries().build();
            pengguna = new Users();
            pengguna.setNama(nama);
            pengguna.setPasswrod(password);
            db.usersDao().insertUser(pengguna);
            daftarView.daftar();
            Log.d("SIZE", "daftar: " + users.size());
            progressDialog.dismiss();
            Toast.makeText(context, "Berhasil Mendaftar", Toast.LENGTH_SHORT).show();

        }
    }
}
