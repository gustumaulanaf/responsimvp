package com.example.jean.retrofitexample.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.jean.retrofitexample.R;
import com.example.jean.retrofitexample.adapter.MainAdapter;
import com.example.jean.retrofitexample.model.history.History;
import com.example.jean.retrofitexample.model.history.HistoryItem;
import com.example.jean.retrofitexample.model.player.Player;
import com.example.jean.retrofitexample.presenter.FootballPresenter;
import com.example.jean.retrofitexample.view.FootBallView;

import java.util.List;

public class MainActivity extends AppCompatActivity implements FootBallView {
    MainAdapter mainAdapter;
    RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        FootballPresenter footballPresenter = new FootballPresenter(this);

        // Maybe it's best to call it on onResume()
        footballPresenter.getPlayerPresenter();
        footballPresenter.getHistory();
    }

    @Override
    public void playerReady(List<Player> players) {

        // See your Logcat :)
        for (Player player : players) {
            Log.d("RETROFIT", "Hail:" + "\n"
                    + " - Title:  " + player.getNama() + " \n"
                    + " - Brief: " + player.getDeskripsi());
        }
    }

    @Override
    public void historyReady(List<History> histories) {
        for (History history : histories) {
            mainAdapter= new MainAdapter(MainActivity.this,histories);
            mainAdapter.setContext(MainActivity.this);
            recyclerView = findViewById(R.id.rvPlayer);
            recyclerView.setHasFixedSize(true);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MainActivity.this,LinearLayoutManager.VERTICAL,false);
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.setAdapter(mainAdapter);


//            Log.d("RETROFIT", "HistoryPemain: " + "\n"
//                    + "NAMA  :" + history.getNama() + "\n"
//                    + "Usia  :" + history.getAge()+"\n"+
//                    "Deskripsi :"+history.getDeskripsi()+"\n");
        }
    }
}
