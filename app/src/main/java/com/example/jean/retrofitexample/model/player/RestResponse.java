package com.example.jean.retrofitexample.model.player;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class RestResponse {

    @SerializedName("")
    private List<String> messages;

    @SerializedName("success")
    private boolean success;

    @SerializedName("data")
    private List<Player> result;

    public List<Player> getResult() {
        return result;
    }

    public List<String> getMessages() {
        return messages;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}