package com.example.jean.retrofitexample.service;

import com.example.jean.retrofitexample.model.history.ResponseHistory;
import com.example.jean.retrofitexample.model.player.RestResponse;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * This class represents the Countries API, all endpoints can stay here.
 *
 * @author Jean Carlos (Github: @jeancsanchez)
 * @date 09/03/18.
 * Jesus loves you.
 */
public interface BaseAPI {
    @GET("player")
    Call<RestResponse> getPlayer();

    @GET("history")
    Call<ResponseHistory> getHistory();
}
