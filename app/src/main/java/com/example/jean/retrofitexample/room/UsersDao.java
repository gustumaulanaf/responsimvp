package com.example.jean.retrofitexample.room;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface UsersDao {
@Query("SELECT * FROM Users")
    List<Users> getAll();
@Query("SELECT * FROM Users WHERE nama LIKE :nama and password LIKE :password ")
List<Users> getUser(String nama,String password);
@Insert
    void insertUser(Users users);

}
