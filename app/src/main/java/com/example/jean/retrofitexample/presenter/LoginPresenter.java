package com.example.jean.retrofitexample.presenter;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.example.jean.retrofitexample.activity.MainActivity;
import com.example.jean.retrofitexample.room.AppDatabase;
import com.example.jean.retrofitexample.room.Users;
import com.example.jean.retrofitexample.view.LoginView;

import java.util.ArrayList;
import java.util.List;

public class LoginPresenter {
    LoginView loginView;
    List<Users> users = new ArrayList<>();
    AppDatabase db;
    public LoginPresenter(LoginView loginView) {
        this.loginView = loginView;
    }



    public  void login(Context context,String nama , String password){
       db= Room.databaseBuilder(context.getApplicationContext(),AppDatabase.class,"user-db").allowMainThreadQueries().build();
       users = db.usersDao().getUser(nama,password);
       if (users.size()>0){
           Toast.makeText(context,"Selamat Datang "+nama,Toast.LENGTH_LONG).show();
           loginView.loginproses();
             }
       else {
           Toast.makeText(context,"Login Failed / User Tidak Terdaftar",Toast.LENGTH_SHORT).show();
       }

    }
}
