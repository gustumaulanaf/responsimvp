package com.example.jean.retrofitexample.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.jean.retrofitexample.R;
import com.example.jean.retrofitexample.presenter.DaftarPresenter;
import com.example.jean.retrofitexample.view.DaftarView;

public class DaftarActivity extends AppCompatActivity implements DaftarView {
    DaftarPresenter daftarPresenter;
    EditText namaDaftar, passwordDaftar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar);
        daftarPresenter = new DaftarPresenter(this);
        namaDaftar = findViewById(R.id.etNamaDaftar);
        passwordDaftar = findViewById(R.id.etPasswordDaftar);

    }

    @Override
    public void daftar() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }


    public void daftarUser(View view) {
        String getNama = namaDaftar.getText().toString();
        String getPassword = passwordDaftar.getText().toString();
        daftarPresenter.daftar(this, getNama, getPassword);
    }
}
